# TelegramRabbitConnector
Receives Telegram Message and forward it to a RabbitMQ exchange

Integration with Telegram is provided by:
  - API available on: https://github.com/tdlib/td
  - Java library from: https://github.com/rubenlagus/TelegramApi
  - Base example Telegram bot Project found on: https://github.com/rubenlagus/Deepthought

