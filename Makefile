DEBUG_5005=-Dspring-boot.run.jvmArguments="-Xdebug -Xrunjdwp:transport=dt_socket,server=y,suspend=y,address=5005"

docker-compose-up:
	docker-compose up -d

run-dev: docker-compose-up
	mvn spring-boot:run -Dspring-boot.run.profiles=dev $(DEBUG_5005)

run-prd:
	mvn spring-boot:run -Dspring-boot.run.profiles=prod