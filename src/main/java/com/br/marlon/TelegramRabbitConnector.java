package com.br.marlon;

import com.br.marlon.telegram.bot.messageforwarder.service.TelegramService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication
@EnableAsync
public class TelegramRabbitConnector implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(TelegramRabbitConnector.class, args);
	}

	@Bean
	public TelegramService getTelegramService(){
		return new TelegramService();
	}

	@Override
	public void run(String... args) {
		getTelegramService().startTelegramBot();
	}

}
