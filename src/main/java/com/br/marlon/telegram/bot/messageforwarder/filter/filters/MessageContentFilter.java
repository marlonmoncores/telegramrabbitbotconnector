package com.br.marlon.telegram.bot.messageforwarder.filter.filters;

import com.br.marlon.telegram.bot.messageforwarder.domain.FatMessage;
import com.br.marlon.telegram.bot.messageforwarder.filter.MessageFilter;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class MessageContentFilter implements MessageFilter {

    private final Set<String> contentList;

    public MessageContentFilter(Set<String> nameList) {
        this.contentList = new HashSet<>(nameList.size());
        nameList.forEach(name -> this.contentList.add(name.toLowerCase()));
    }

    @Override
    public boolean checkFilterPass(FatMessage message) {
        List<String> listToSearch = new ArrayList<>();

        addElementLowerCaseToListIfNotNull(listToSearch, message.getMessage());

        if(message.getMessage() != null) {
            String messageLC = message.getMessage().toLowerCase();
            return contentList.stream().anyMatch(value -> messageLC.contains(value));
        }
        return false;
    }

    private void addElementLowerCaseToListIfNotNull(List<String> listToAdd, String element){
        if(element != null){
            listToAdd.add(element.toLowerCase());
        }
    }
}
