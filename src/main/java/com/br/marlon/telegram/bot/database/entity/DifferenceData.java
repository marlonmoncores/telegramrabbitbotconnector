package com.br.marlon.telegram.bot.database.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;

@RedisHash("DifferenceData")
public class DifferenceData {

    @Id
    private Integer id;
    private Integer pts;
    private Integer date;
    private Integer seq;

    public DifferenceData() {
    }

    public DifferenceData(Integer id, Integer pts, Integer date, Integer seq) {
        this.id = id;
        this.pts = pts;
        this.date = date;
        this.seq = seq;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getPts() {
        return pts;
    }

    public void setPts(Integer pts) {
        this.pts = pts;
    }

    public Integer getDate() {
        return date;
    }

    public void setDate(Integer date) {
        this.date = date;
    }

    public Integer getSeq() {
        return seq;
    }

    public void setSeq(Integer seq) {
        this.seq = seq;
    }
}
