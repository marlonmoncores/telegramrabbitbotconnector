package com.br.marlon.telegram.bot.messageforwarder.handler;

import com.br.marlon.telegram.bot.messageforwarder.database.DatabaseManagerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.telegram.bot.ChatUpdatesBuilder;
import org.telegram.bot.handlers.UpdatesHandlerBase;
import org.telegram.bot.kernel.IKernelComm;
import org.telegram.bot.kernel.database.DatabaseManager;
import org.telegram.bot.kernel.differenceparameters.IDifferenceParametersService;

@Component
public class MessageUpdatesHandlerBuilder implements ChatUpdatesBuilder {

    @Autowired
    private DatabaseManagerService databaseManager;

    @Value("${telegram.handler:MESSAGE}")
    private String name;

    private IKernelComm kernelComm;
    private IDifferenceParametersService differenceParametersService;

    @Override
    public void setKernelComm(IKernelComm kernelComm) {
        this.kernelComm = kernelComm;
    }

    @Override
    public void setDifferenceParametersService(IDifferenceParametersService differenceParametersService) {
        this.differenceParametersService = differenceParametersService;
    }

    @Override
    public DatabaseManager getDatabaseManager() {
        return databaseManager;
    }

    @Override
    public UpdatesHandlerBase build() {
        if (kernelComm == null) {
            throw new NullPointerException("Can't build the handler without a KernelComm");
        }
        if (differenceParametersService == null) {
            throw new NullPointerException("Can't build the handler without a differenceParamtersService");
        }

        return UpdatesHandlerFactory.createHandlerForName(kernelComm, differenceParametersService, databaseManager, name);
    }
}
