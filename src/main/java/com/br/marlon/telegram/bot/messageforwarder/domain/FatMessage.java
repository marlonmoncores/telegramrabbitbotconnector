package com.br.marlon.telegram.bot.messageforwarder.domain;

import com.br.marlon.telegram.bot.database.entity.Chat;
import com.br.marlon.telegram.bot.database.entity.User;

public class FatMessage {
    private User fromUser;
    private Chat chat;
    private Chat channel;
    private String message;
    private int date;
    private Integer viaBotId;
    private Integer replyToMsgId;

    public boolean isChatMessage(){
        return chat != null;
    }

    public boolean isChannelMessage(){
        return channel != null;
    }

    public boolean isPersonalMessage(){
        return chat == null && channel == null;
    }

    public User getFromUser() {
        return fromUser;
    }

    public void setFromUser(User fromUser) {
        this.fromUser = fromUser;
    }

    public Chat getChat() {
        return chat;
    }

    public void setChat(Chat chat) {
        this.chat = chat;
    }

    public Chat getChannel() {
        return channel;
    }

    public void setChannel(Chat channel) {
        this.channel = channel;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getDate() {
        return date;
    }

    public void setDate(int date) {
        this.date = date;
    }

    public Integer getViaBotId() {
        return viaBotId;
    }

    public void setViaBotId(Integer viaBotId) {
        this.viaBotId = viaBotId;
    }

    public Integer getReplyToMsgId() {
        return replyToMsgId;
    }

    public void setReplyToMsgId(Integer replyToMsgId) {
        this.replyToMsgId = replyToMsgId;
    }
}
