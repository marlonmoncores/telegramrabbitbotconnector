package com.br.marlon.telegram.bot.messageforwarder.handler;

import com.br.marlon.telegram.bot.messageforwarder.database.DatabaseManagerService;
import org.telegram.bot.handlers.UpdatesHandlerBase;
import org.telegram.bot.kernel.IKernelComm;
import org.telegram.bot.kernel.database.DatabaseManager;
import org.telegram.bot.kernel.differenceparameters.IDifferenceParametersService;

public class UpdatesHandlerFactory {

    public static UpdatesHandlerBase createHandlerForName(IKernelComm kernelComm, IDifferenceParametersService differenceParametersService, DatabaseManagerService databaseManager, String name){
        switch (name){
            case "LOG": return new LogEverithingHandler(kernelComm, differenceParametersService, databaseManager);
            default: return new MessageForwarderHandlerManager(kernelComm, differenceParametersService, databaseManager);
        }
    }
}
