package com.br.marlon.telegram.bot.messageforwarder.service;

import com.br.marlon.telegram.bot.database.repository.ChatRepository;
import com.br.marlon.telegram.bot.database.repository.UserRepository;
import com.br.marlon.telegram.bot.messageforwarder.domain.FatMessage;
import com.br.marlon.telegram.bot.messageforwarder.domain.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FatMessageService {

    @Autowired
    private ChatRepository chatRepository;

    @Autowired
    private UserRepository userRepository;


    public FatMessage build(Message message){
        FatMessage fatMessage = new FatMessage();
        fatMessage.setReplyToMsgId(message.getReplyToMsgId());
        fatMessage.setViaBotId(message.getViaBotId());
        fatMessage.setChannel(message.getChannelId() != null ? chatRepository.findById(message.getChannelId()).orElse(null) : null);
        fatMessage.setChat(message.getChatId() != null ? chatRepository.findById(message.getChatId()).orElse(null) : null);
        fatMessage.setFromUser(message.getChatId() != null ? userRepository.findById(message.getFromUserId()).orElse(null) : null);
        fatMessage.setDate(message.getDate());
        fatMessage.setMessage(message.getMessage());

        return fatMessage;
    }
}
