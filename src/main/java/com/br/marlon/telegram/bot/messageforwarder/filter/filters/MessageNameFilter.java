package com.br.marlon.telegram.bot.messageforwarder.filter.filters;

import com.br.marlon.telegram.bot.messageforwarder.domain.FatMessage;
import com.br.marlon.telegram.bot.messageforwarder.filter.MessageFilter;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class MessageNameFilter implements MessageFilter {

    private final Set<String> nameList;

    public MessageNameFilter(Set<String> nameList) {
        this.nameList = new HashSet<>(nameList.size());
        nameList.forEach(name -> this.nameList.add(name.toLowerCase()));
    }

    @Override
    public boolean checkFilterPass(FatMessage message) {
        List<String> listToSearch = new ArrayList<>();

        if(message.getChat() != null){
            addElementLowerCaseToListIfNotNull(listToSearch, message.getChat().getTitle());
            addElementLowerCaseToListIfNotNull(listToSearch, message.getChat().getUsername());
        }
        if(message.getChannel() != null){
            addElementLowerCaseToListIfNotNull(listToSearch, message.getChannel().getTitle());
            addElementLowerCaseToListIfNotNull(listToSearch, message.getChannel().getUsername());
        }
        if(message.getFromUser() != null) {
            addElementLowerCaseToListIfNotNull(listToSearch, message.getFromUser().getUserName());
            addElementLowerCaseToListIfNotNull(listToSearch, message.getFromUser().getFirstName()+" "+message.getFromUser().getLastName());
        }

        return listToSearch.stream().anyMatch(value -> nameList.contains(value));
    }

    private void addElementLowerCaseToListIfNotNull(List<String> listToAdd, String element){
        if(element != null){
            listToAdd.add(element.toLowerCase());
        }
    }
}
