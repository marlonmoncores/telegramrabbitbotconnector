package com.br.marlon.telegram.bot.database.repository;

import com.br.marlon.telegram.bot.database.entity.DifferenceData;
import org.springframework.data.repository.CrudRepository;

public interface DifferenceDataRepository extends CrudRepository<DifferenceData, Integer> {
    
}
