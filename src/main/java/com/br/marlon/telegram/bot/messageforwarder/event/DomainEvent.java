package com.br.marlon.telegram.bot.messageforwarder.event;

import java.io.Serializable;

public class DomainEvent<T> implements Serializable {
    private T eventData;

    public DomainEvent(T eventData) {
        this.eventData = eventData;
    }

    public T getEventData() {
        return eventData;
    }
}
