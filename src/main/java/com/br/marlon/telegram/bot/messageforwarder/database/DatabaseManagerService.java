package com.br.marlon.telegram.bot.messageforwarder.database;

import com.br.marlon.telegram.bot.database.entity.Chat;
import com.br.marlon.telegram.bot.database.entity.DifferenceData;
import com.br.marlon.telegram.bot.database.entity.User;
import com.br.marlon.telegram.bot.database.repository.ChatRepository;
import com.br.marlon.telegram.bot.database.repository.DifferenceDataRepository;
import com.br.marlon.telegram.bot.database.repository.UserRepository;
import com.br.marlon.telegram.bot.messageforwarder.domain.TLChat;
import com.br.marlon.telegram.bot.messageforwarder.domain.TLUser;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.telegram.bot.kernel.database.DatabaseManager;
import org.telegram.bot.structure.IUser;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Service
public class DatabaseManagerService implements DatabaseManager {

    @Autowired
    private ChatRepository chatRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private DifferenceDataRepository differenceDataRepository;

    public void save(Chat chat){
        chatRepository.save(chat);
    }

    public void save(User user){
        userRepository.save(user);
    }

    @Override
    public @Nullable org.telegram.bot.structure.Chat getChatById(int id) {
        Optional<Chat> chatOptional = chatRepository.findById(id);
        if(chatOptional.isPresent()){
            Chat chat = chatOptional.get();
            return new TLChat(chat.getId(), chat.getAccessHash(), chat.isChannel());
        }
        return null;
    }

    @Override
    public @Nullable IUser getUserById(int id) {
        Optional<User> userOptional = userRepository.findById(id);
        if(userOptional.isPresent()){
            User user = userOptional.get();
            return new TLUser(user.getId(), user.getUserHash());
        }
        return null;
    }

    @Override
    public @NotNull Map<Integer, int[]> getDifferencesData() {
        final HashMap<Integer, int[]> differencesDatas = new HashMap<>();
        differenceDataRepository.findAll().forEach(result ->{
            if(result != null) {
                final int[] differencesData = new int[3];
                differencesData[0] = result.getPts();
                differencesData[1] = result.getDate();
                differencesData[2] = result.getSeq();
                differencesDatas.put(result.getId(), differencesData);
            }
        });
        return differencesDatas;
    }

    @Override
    public boolean updateDifferencesData(int botId, int pts, int date, int seq) {
        differenceDataRepository.save(new DifferenceData(botId, pts, date, seq));
        return true;
    }
}
