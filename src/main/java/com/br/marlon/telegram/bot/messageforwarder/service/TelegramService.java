package com.br.marlon.telegram.bot.messageforwarder.service;

import com.br.marlon.telegram.ExternalAuthFileBotConfig;
import com.br.marlon.telegram.bot.messageforwarder.handler.MessageUpdatesHandlerBuilder;
import com.br.marlon.telegram.configuration.TelegramProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.telegram.bot.kernel.TelegramBot;
import org.telegram.bot.services.BotLogger;
import org.telegram.bot.structure.BotConfig;
import org.telegram.bot.structure.LoginStatus;

@Service
public class TelegramService {
    private static final org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger(TelegramService.class);

    @Autowired
    TelegramProperties telegramProperties;

    @Autowired
    MessageUpdatesHandlerBuilder handler;


    public void startTelegramBot() {
        log.info("Initializing bot for msisdn {}",telegramProperties.getMsisdn());
        final BotConfig botConfig = new ExternalAuthFileBotConfig(telegramProperties.getAuthFilePath(), telegramProperties.getMsisdn());

        try {
            final TelegramBot kernel = new TelegramBot(botConfig, handler, telegramProperties.getApiKey(), telegramProperties.getApiHash());
            LoginStatus status = kernel.init();
            if (status == LoginStatus.CODESENT) {
                log.warn("Bot needs to log in");
                if(StringUtils.isEmpty(telegramProperties.getAuthCode())){
                    log.error("Please insert the code sent to your telegram to authorize the bot");
                    return;
                }
                boolean success = kernel.getKernelAuth().setAuthCode(telegramProperties.getAuthCode());
                log.info("Successful login");
                if (success) {
                    status = LoginStatus.ALREADYLOGGED;
                }
            }
            if (status == LoginStatus.ALREADYLOGGED) {
                kernel.startBot();
                log.info("Bot is ready");
            } else {
                log.error("Failed to log in. Status is: {}",  status);
                throw new Exception("Failed to log in. Status is: " + status);
            }
        } catch (Exception e) {
            log.error("Error initializing bot", e);
            BotLogger.severe("MAIN", e);
            throw new RuntimeException(e);
        }
    }
}
