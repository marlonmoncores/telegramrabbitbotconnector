package com.br.marlon.telegram.bot.messageforwarder.handler;

import com.br.marlon.telegram.bot.messageforwarder.database.DatabaseManagerService;
import com.br.marlon.telegram.bot.messageforwarder.handler.managed.ChatHandler;
import com.br.marlon.telegram.bot.messageforwarder.handler.managed.MessageHandler;
import com.br.marlon.telegram.bot.messageforwarder.handler.managed.UserHandler;
import org.telegram.api.chat.TLAbsChat;
import org.telegram.api.message.TLAbsMessage;
import org.telegram.api.update.TLUpdateChannelNewMessage;
import org.telegram.api.update.TLUpdateEditChannelMessage;
import org.telegram.api.update.TLUpdateEditMessage;
import org.telegram.api.update.TLUpdateNewMessage;
import org.telegram.api.updates.TLUpdateShortChatMessage;
import org.telegram.api.updates.TLUpdateShortMessage;
import org.telegram.api.user.TLAbsUser;
import org.telegram.bot.handlers.DefaultUpdatesHandler;
import org.telegram.bot.kernel.IKernelComm;
import org.telegram.bot.kernel.differenceparameters.IDifferenceParametersService;

import java.util.List;


public class MessageForwarderHandlerManager extends DefaultUpdatesHandler {
    private final String LOGTAG = "MessageForwarderHandlerManager";

    private MessageHandler messageHandler;
    private ChatHandler chatHandler;
    private UserHandler userHandler;

    public MessageForwarderHandlerManager(IKernelComm kernelComm, IDifferenceParametersService differenceParametersService, DatabaseManagerService databaseManager) {
        super(kernelComm, differenceParametersService, databaseManager);
        this.messageHandler = new MessageHandler();
        this.chatHandler = new ChatHandler(databaseManager);
        this.userHandler = new UserHandler(databaseManager);
    }

    @Override
    protected void onUsersCustom(List<TLAbsUser> users) {
        userHandler.onUsers(users);
    }

    @Override
    protected void onChatsCustom(List<TLAbsChat> chats) {
        chatHandler.onChats(chats);
    }

    @Override
    public void onTLUpdateShortMessageCustom(TLUpdateShortMessage update) {
        messageHandler.onMessage(update);
    }

    @Override
    public void onTLUpdateNewMessageCustom(TLUpdateNewMessage update) {
        messageHandler.onMessage(update);
    }

    @Override
    protected void onTLAbsMessageCustom(TLAbsMessage message) {
        messageHandler.onMessage(message);
    }

    @Override
    protected void onTLUpdateShortChatMessageCustom(TLUpdateShortChatMessage message) {
        messageHandler.onMessage(message);
    }

    @Override
    protected void onTLUpdateChannelNewMessageCustom(TLUpdateChannelNewMessage update) {
        messageHandler.onMessage(update);
    }

    @Override
    protected void onTLUpdateEditChannelMessageCustom(TLUpdateEditChannelMessage tlUpdateEditChannelMessage) {
        messageHandler.onMessage(tlUpdateEditChannelMessage);
    }

    @Override
    protected void onTLUpdateEditMessageCustom(TLUpdateEditMessage tlUpdateEditMessage) {
        messageHandler.onMessage(tlUpdateEditMessage);
    }



}
