package com.br.marlon.telegram.bot.messageforwarder.event;

import com.br.marlon.telegram.bot.messageforwarder.domain.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;

@Component
public class MessageForwarderEventPublisher {

    @Autowired
    private ApplicationEventPublisher applicationEventPublisher;


    public void publishMessage(Message message){
        applicationEventPublisher.publishEvent(new MessageForwarderEvent(message));
    }

}
