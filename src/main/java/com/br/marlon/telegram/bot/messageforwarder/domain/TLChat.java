package com.br.marlon.telegram.bot.messageforwarder.domain;

import org.telegram.bot.structure.Chat;

public class TLChat implements Chat {

    private int id;
    private Long accessHash;
    private boolean channel;


    public TLChat(int id, Long accessHash, boolean channel) {
        this.id = id;
        this.accessHash = accessHash;
        this.channel = channel;
    }

    @Override
    public int getId() {
        return this.id;
    }

    @Override
    public Long getAccessHash() {
        return this.accessHash;
    }

    @Override
    public boolean isChannel() {
        return this.channel;
    }
}
