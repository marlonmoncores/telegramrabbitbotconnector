package com.br.marlon.telegram.bot.messageforwarder.domain;

import org.telegram.bot.structure.IUser;

public class TLUser implements IUser {

    private int id;
    private Long accessHash;


    public TLUser(int id, Long accessHash) {
        this.id = id;
        this.accessHash = accessHash;
    }

    @Override
    public int getUserId() {
        return 0;
    }

    @Override
    public Long getUserHash() {
        return null;
    }
}
