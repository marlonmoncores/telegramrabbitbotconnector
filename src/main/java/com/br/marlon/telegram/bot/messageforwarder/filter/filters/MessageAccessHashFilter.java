package com.br.marlon.telegram.bot.messageforwarder.filter.filters;

import com.br.marlon.telegram.bot.messageforwarder.domain.FatMessage;
import com.br.marlon.telegram.bot.messageforwarder.filter.MessageFilter;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class MessageAccessHashFilter implements MessageFilter {

    private final Set<Long> accessHashList;

    public MessageAccessHashFilter(Set<Long> accessHashList) {
        this.accessHashList = accessHashList;
    }


    @Override
    public boolean checkFilterPass(FatMessage message) {
        List<Long> listToSearch = new ArrayList<>();

        if(message.getChat() != null){
            listToSearch.add(message.getChat().getAccessHash());
        }
        if(message.getChannel() != null){
            listToSearch.add(message.getChannel().getAccessHash());
        }
        if(message.getFromUser() != null) {
            listToSearch.add(message.getFromUser().getUserHash());
        }

        return listToSearch.stream().anyMatch(value -> accessHashList.contains(value));
    }
}
