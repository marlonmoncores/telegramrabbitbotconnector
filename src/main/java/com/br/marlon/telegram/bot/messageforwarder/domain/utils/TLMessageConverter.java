package com.br.marlon.telegram.bot.messageforwarder.domain.utils;

import com.br.marlon.telegram.bot.messageforwarder.domain.Message;
import org.telegram.api.message.TLMessage;
import org.telegram.api.updates.TLUpdateShortChatMessage;
import org.telegram.api.updates.TLUpdateShortMessage;
import org.telegram.tl.TLObject;

import java.util.Optional;

public class TLMessageConverter {

    public Optional<Message> convertMessage(TLObject tlObject){
        if(tlObject instanceof TLMessage){
            return Optional.of(convertMessage((TLMessage) tlObject));
        }else if(tlObject instanceof TLUpdateShortChatMessage){
            return Optional.of(convertMessage((TLUpdateShortChatMessage) tlObject));
        }else if(tlObject instanceof TLUpdateShortMessage){
            return Optional.of(convertMessage((TLUpdateShortMessage) tlObject));
        }else{
            return Optional.empty();
        }
    }

    private Message convertMessage(TLUpdateShortChatMessage tlMessage){
        Message message = new Message();
        message.setId(tlMessage.getId());
        message.setChatId(tlMessage.getChatId());
        message.setFromUserId(tlMessage.getFromId());
        message.setMessage(tlMessage.getMessage());
        message.setDate(tlMessage.getDate());
        message.setReplyToMsgId(tlMessage.getReplyToMsgId());
        return message;
    }

    private Message convertMessage(TLUpdateShortMessage tlMessage){
        Message message = new Message();
        message.setId(tlMessage.getId());
        message.setFromUserId(tlMessage.getUserId());
        message.setMessage(tlMessage.getMessage());
        message.setDate(tlMessage.getDate());
        message.setViaBotId(tlMessage.getViaBotId());
        message.setReplyToMsgId(tlMessage.getReplyToMsgId());
        return message;
    }

    private Message convertMessage(TLMessage tlMessage){
        Message message = new Message();
        message.setId(tlMessage.getId());
        message.setFromUserId(tlMessage.getFromId());
        message.setMessage(tlMessage.getMessage());
        message.setDate(tlMessage.getDate());
        message.setViaBotId(tlMessage.getViaBotId());
        message.setReplyToMsgId(tlMessage.getReplyToMsgId());
        if(tlMessage.getToId() != null) {
            message.setChannelId(tlMessage.getToId().getId());
        }
        return message;
    }
}
