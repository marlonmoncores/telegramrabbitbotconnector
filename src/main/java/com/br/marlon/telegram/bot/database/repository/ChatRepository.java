package com.br.marlon.telegram.bot.database.repository;

import com.br.marlon.telegram.bot.database.entity.Chat;
import org.springframework.data.repository.CrudRepository;

public interface ChatRepository extends CrudRepository<Chat, Integer> {

}
