package com.br.marlon.telegram.bot.event;

import com.br.marlon.telegram.bot.messageforwarder.domain.FatMessage;
import com.br.marlon.telegram.bot.messageforwarder.event.MessageForwarderEvent;
import com.br.marlon.telegram.bot.messageforwarder.service.FatMessageService;
import com.br.marlon.telegram.bot.messageforwarder.service.MessageFilterService;
import com.br.marlon.telegram.bot.publisher.TLMessagePublisher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.lang.NonNull;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

@Component
public class MessageForwarderEventListener {

    @Autowired
    private FatMessageService fatMessageService;

    @Autowired
    private TLMessagePublisher tlMessagePublisher;

    @Autowired
    private MessageFilterService messageFilterService;

    @EventListener
    @Async
    public void messageArrivedEventListener(@NonNull MessageForwarderEvent messageEvent){
        FatMessage fatMessage = fatMessageService.build(messageEvent.getEventData());
        if(messageFilterService.checkFilterPass(fatMessage)) {
            tlMessagePublisher.messageArrived().send(
                    MessageBuilder.withPayload(fatMessage)
                            .build()
            );
        }
    }
}
