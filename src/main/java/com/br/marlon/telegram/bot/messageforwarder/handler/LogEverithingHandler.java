package com.br.marlon.telegram.bot.messageforwarder.handler;

import org.telegram.api.chat.TLAbsChat;
import org.telegram.api.message.TLAbsMessage;
import org.telegram.api.update.*;
import org.telegram.api.update.encrypted.TLUpdateEncryptedChatTyping;
import org.telegram.api.update.encrypted.TLUpdateEncryptedMessagesRead;
import org.telegram.api.update.encrypted.TLUpdateEncryption;
import org.telegram.api.update.encrypted.TLUpdateNewEncryptedMessage;
import org.telegram.api.updates.TLUpdateShortChatMessage;
import org.telegram.api.updates.TLUpdateShortMessage;
import org.telegram.api.updates.TLUpdateShortSentMessage;
import org.telegram.api.user.TLAbsUser;
import org.telegram.bot.handlers.UpdatesHandlerBase;
import org.telegram.bot.kernel.IKernelComm;
import org.telegram.bot.kernel.database.DatabaseManager;
import org.telegram.bot.kernel.differenceparameters.IDifferenceParametersService;

import java.util.List;

/**
 * @author Ruben Bermudez
 * @version 1.0
 * @brief TODO
 * @date 16 of October of 2016
 */
public class LogEverithingHandler extends UpdatesHandlerBase {
        private static final org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger(LogEverithingHandler.class);

    public LogEverithingHandler(IKernelComm kernelComm, IDifferenceParametersService differenceParametersService, DatabaseManager databaseManager) {
        super(kernelComm, differenceParametersService, databaseManager);
    }

    private void logTLEvent(String name, Object obj){
        log.info(name, obj);
    }

    @Override
    protected void onTLUpdateChatParticipantsCustom(TLUpdateChatParticipants tlUpdateChatParticipants) {
        logTLEvent("onTLUpdateChatParticipantsCustom",tlUpdateChatParticipants);
    }

    @Override
    protected void onTLUpdateNewMessageCustom(TLUpdateNewMessage tlUpdateNewMessage) {
        logTLEvent("onTLUpdateNewMessageCustom",tlUpdateNewMessage);
    }

    @Override
    protected void onTLUpdateChannelNewMessageCustom(TLUpdateChannelNewMessage tlUpdateChannelNewMessage) {
        logTLEvent("onTLUpdateChannelNewMessageCustom",tlUpdateChannelNewMessage);
    }

    @Override
    protected void onTLUpdateChannelCustom(TLUpdateChannel tlUpdateChannel) {
        logTLEvent("onTLUpdateChannelCustom",tlUpdateChannel);
    }

    @Override
    protected void onTLUpdateBotInlineQueryCustom(TLUpdateBotInlineQuery tlUpdateBotInlineQuery) {
        logTLEvent("onTLUpdateBotInlineQueryCustom",tlUpdateBotInlineQuery);
    }

    @Override
    protected void onTLUpdateBotInlineSendCustom(TLUpdateBotInlineSend tlUpdateBotInlineSend) {
        logTLEvent("onTLUpdateBotInlineSendCustom",tlUpdateBotInlineSend);
    }

    @Override
    protected void onTLUpdateChannelMessageViewsCustom(TLUpdateChannelMessageViews tlUpdateChannelMessageViews) {
        logTLEvent("onTLUpdateChannelMessageViewsCustom",tlUpdateChannelMessageViews);
    }

    @Override
    protected void onTLUpdateChannelPinnedMessageCustom(TLUpdateChannelPinnedMessage tlUpdateChannelPinnedMessage) {
        logTLEvent("onTLUpdateChannelPinnedMessageCustom",tlUpdateChannelPinnedMessage);
    }

    @Override
    protected void onTLUpdateChatAdminCustom(TLUpdateChatAdmin tlUpdateChatAdmin) {
        logTLEvent("onTLUpdateChatAdminCustom",tlUpdateChatAdmin);
    }

    @Override
    protected void onTLUpdateChatParticipantAddCustom(TLUpdateChatParticipantAdd tlUpdateChatParticipantAdd) {
        logTLEvent("onTLUpdateChatParticipantAddCustom",tlUpdateChatParticipantAdd);
    }

    @Override
    protected void onTLUpdateChatParticipantAdminCustom(TLUpdateChatParticipantAdmin tlUpdateChatParticipantAdmin) {
        logTLEvent("onTLUpdateChatParticipantAdminCustom",tlUpdateChatParticipantAdmin);
    }

    @Override
    protected void onTLUpdateChatParticipantDeleteCustom(TLUpdateChatParticipantDelete tlUpdateChatParticipantDelete) {
        logTLEvent("onTLUpdateChatParticipantDeleteCustom",tlUpdateChatParticipantDelete);
    }

    @Override
    protected void onTLUpdateChatUserTypingCustom(TLUpdateChatUserTyping tlUpdateChatUserTyping) {
        logTLEvent("onTLUpdateChatUserTypingCustom",tlUpdateChatUserTyping);
    }

    @Override
    protected void onTLUpdateContactLinkCustom(TLUpdateContactLink tlUpdateContactLink) {
        logTLEvent("onTLUpdateContactLinkCustom",tlUpdateContactLink);
    }

    @Override
    protected void onTLUpdateContactRegisteredCustom(TLUpdateContactRegistered tlUpdateContactRegistered) {
        logTLEvent("onTLUpdateContactRegisteredCustom",tlUpdateContactRegistered);
    }

    @Override
    protected void onTLUpdateDcOptionsCustom(TLUpdateDcOptions tlUpdateDcOptions) {
        logTLEvent("onTLUpdateDcOptionsCustom",tlUpdateDcOptions);
    }

    @Override
    protected void onTLUpdateDeleteChannelMessagesCustom(TLUpdateDeleteChannelMessages tlUpdateDeleteChannelMessages) {
        logTLEvent("onTLUpdateDeleteChannelMessagesCustom",tlUpdateDeleteChannelMessages);
    }

    @Override
    protected void onTLUpdateDeleteMessagesCustom(TLUpdateDeleteMessages tlUpdateDeleteMessages) {
        logTLEvent("onTLUpdateDeleteMessagesCustom",tlUpdateDeleteMessages);
    }

    @Override
    protected void onTLUpdateEditChannelMessageCustom(TLUpdateEditChannelMessage tlUpdateEditChannelMessage) {
        logTLEvent("onTLUpdateEditChannelMessageCustom",tlUpdateEditChannelMessage);
    }

    @Override
    protected void onTLUpdateMessageIdCustom(TLUpdateMessageId tlUpdateMessageId) {
        logTLEvent("onTLUpdateMessageIdCustom",tlUpdateMessageId);
    }

    @Override
    protected void onTLUpdateNewStickerSetCustom(TLUpdateNewStickerSet tlUpdateNewStickerSet) {
        logTLEvent("onTLUpdateNewStickerSetCustom",tlUpdateNewStickerSet);
    }

    @Override
    protected void onTLUpdateNotifySettingsCustom(TLUpdateNotifySettings tlUpdateNotifySettings) {
        logTLEvent("onTLUpdateNotifySettingsCustom",tlUpdateNotifySettings);
    }

    @Override
    protected void onTLUpdatePrivacyCustom(TLUpdatePrivacy tlUpdatePrivacy) {
        logTLEvent("onTLUpdatePrivacyCustom",tlUpdatePrivacy);
    }

    @Override
    protected void onTLUpdateReadChannelInboxCustom(TLUpdateReadChannelInbox tlUpdateReadChannelInbox) {
        logTLEvent("onTLUpdateReadChannelInboxCustom",tlUpdateReadChannelInbox);
    }

    @Override
    protected void onTLUpdateReadMessagesContentsCustom(TLUpdateReadMessagesContents tlUpdateReadMessagesContents) {
        logTLEvent("onTLUpdateReadMessagesContentsCustom",tlUpdateReadMessagesContents);
    }

    @Override
    protected void onTLUpdateReadMessagesInboxCustom(TLUpdateReadMessagesInbox tlUpdateReadMessagesInbox) {
        logTLEvent("onTLUpdateReadMessagesInboxCustom",tlUpdateReadMessagesInbox);
    }

    @Override
    protected void onTLUpdateReadMessagesOutboxCustom(TLUpdateReadMessagesOutbox tlUpdateReadMessagesOutbox) {
        logTLEvent("onTLUpdateReadMessagesOutboxCustom",tlUpdateReadMessagesOutbox);
    }

    @Override
    protected void onTLUpdateSavedGifsCustom(TLUpdateSavedGifs tlUpdateSavedGifs) {
        logTLEvent("onTLUpdateSavedGifsCustom",tlUpdateSavedGifs);
    }

    @Override
    protected void onTLUpdateServiceNotificationCustom(TLUpdateServiceNotification tlUpdateServiceNotification) {
        logTLEvent("onTLUpdateServiceNotificationCustom",tlUpdateServiceNotification);
    }

    @Override
    protected void onTLUpdateStickerSetsCustom(TLUpdateStickerSets tlUpdateStickerSets) {
        logTLEvent("onTLUpdateStickerSetsCustom",tlUpdateStickerSets);
    }

    @Override
    protected void onTLUpdateStickerSetsOrderCustom(TLUpdateStickerSetsOrder tlUpdateStickerSetsOrder) {
        logTLEvent("onTLUpdateStickerSetsOrderCustom",tlUpdateStickerSetsOrder);
    }

    @Override
    protected void onTLUpdateUserBlockedCustom(TLUpdateUserBlocked tlUpdateUserBlocked) {
        logTLEvent("onTLUpdateUserBlockedCustom",tlUpdateUserBlocked);
    }

    @Override
    protected void onTLUpdateUserNameCustom(TLUpdateUserName tlUpdateUserName) {
        logTLEvent("onTLUpdateUserNameCustom",tlUpdateUserName);
    }

    @Override
    protected void onTLUpdateUserPhoneCustom(TLUpdateUserPhone tlUpdateUserPhone) {
        logTLEvent("onTLUpdateUserPhoneCustom",tlUpdateUserPhone);
    }

    @Override
    protected void onTLUpdateUserPhotoCustom(TLUpdateUserPhoto tlUpdateUserPhoto) {
        logTLEvent("onTLUpdateUserPhotoCustom",tlUpdateUserPhoto);
    }

    @Override
    protected void onTLUpdateUserStatusCustom(TLUpdateUserStatus tlUpdateUserStatus) {
        logTLEvent("onTLUpdateUserStatusCustom",tlUpdateUserStatus);
    }

    @Override
    protected void onTLUpdateUserTypingCustom(TLUpdateUserTyping tlUpdateUserTyping) {
        logTLEvent("onTLUpdateUserTypingCustom",tlUpdateUserTyping);
    }

    @Override
    protected void onTLUpdateWebPageCustom(TLUpdateWebPage tlUpdateWebPage) {
        logTLEvent("onTLUpdateWebPageCustom",tlUpdateWebPage);
    }

    @Override
    protected void onTLFakeUpdateCustom(TLFakeUpdate tlFakeUpdate) {
        logTLEvent("onTLFakeUpdateCustom",tlFakeUpdate);
    }

    @Override
    protected void onTLUpdateShortMessageCustom(TLUpdateShortMessage tlUpdateShortMessage) {
        logTLEvent("onTLUpdateShortMessageCustom",tlUpdateShortMessage);
    }

    @Override
    protected void onTLUpdateShortChatMessageCustom(TLUpdateShortChatMessage tlUpdateShortChatMessage) {
        logTLEvent("onTLUpdateShortChatMessageCustom",tlUpdateShortChatMessage);
    }

    @Override
    protected void onTLUpdateShortSentMessageCustom(TLUpdateShortSentMessage tlUpdateShortSentMessage) {
        logTLEvent("onTLUpdateShortSentMessageCustom",tlUpdateShortSentMessage);
    }

    @Override
    protected void onTLUpdateBotCallbackQueryCustom(TLUpdateBotCallbackQuery tlUpdateBotCallbackQuery) {
        logTLEvent("onTLUpdateBotCallbackQueryCustom",tlUpdateBotCallbackQuery);
    }

    @Override
    protected void onTLUpdateEditMessageCustom(TLUpdateEditMessage tlUpdateEditMessage) {
        logTLEvent("onTLUpdateEditMessageCustom",tlUpdateEditMessage);
    }

    @Override
    protected void onTLUpdateInlineBotCallbackQueryCustom(TLUpdateInlineBotCallbackQuery tlUpdateInlineBotCallbackQuery) {
        logTLEvent("onTLUpdateInlineBotCallbackQueryCustom",tlUpdateInlineBotCallbackQuery);
    }

    @Override
    protected void onTLAbsMessageCustom(TLAbsMessage tlAbsMessage) {
        logTLEvent("onTLAbsMessageCustom",tlAbsMessage);
    }

    @Override
    protected void onUsersCustom(List<TLAbsUser> list) {
        logTLEvent("onUsersCustom",list);
    }

    @Override
    protected void onChatsCustom(List<TLAbsChat> list) {
        logTLEvent("onChatsCustom",list);
    }

    @Override
    protected void onTLUpdateEncryptionCustom(TLUpdateEncryption tlUpdateEncryption) {
        logTLEvent("onTLUpdateEncryptionCustom",tlUpdateEncryption);
    }

    @Override
    protected void onTLUpdateEncryptedMessagesReadCustom(TLUpdateEncryptedMessagesRead tlUpdateEncryptedMessagesRead) {
        logTLEvent("onTLUpdateEncryptedMessagesReadCustom",tlUpdateEncryptedMessagesRead);
    }

    @Override
    protected void onTLUpdateNewEncryptedMessageCustom(TLUpdateNewEncryptedMessage tlUpdateNewEncryptedMessage) {
        logTLEvent("onTLUpdateNewEncryptedMessageCustom",tlUpdateNewEncryptedMessage);
    }

    @Override
    protected void onTLUpdateEncryptedChatTypingCustom(TLUpdateEncryptedChatTyping tlUpdateEncryptedChatTyping) {
        logTLEvent("onTLUpdateEncryptedChatTypingCustom",tlUpdateEncryptedChatTyping);
    }

    @Override
    protected void onTLUpdateConfigCustom(TLUpdateConfig tlUpdateConfig) {
        logTLEvent("onTLUpdateConfigCustom",tlUpdateConfig);
    }

    @Override
    protected void onTLUpdateDraftMessageCustom(TLUpdateDraftMessage tlUpdateDraftMessage) {
        logTLEvent("onTLUpdateDraftMessageCustom",tlUpdateDraftMessage);
    }

    @Override
    protected void onTLUpdatePtsChangedCustom(TLUpdatePtsChanged tlUpdatePtsChanged) {
        logTLEvent("onTLUpdatePtsChangedCustom",tlUpdatePtsChanged);
    }

    @Override
    protected void onTLUpdateReadChannelOutboxCustom(TLUpdateReadChannelOutbox tlUpdateReadChannelOutbox) {
        logTLEvent("onTLUpdateReadChannelOutboxCustom",tlUpdateReadChannelOutbox);
    }

    @Override
    protected void onTLUpdateReadFeaturedStickersCustom(TLUpdateReadFeaturedStickers tlUpdateReadFeaturedStickers) {
        logTLEvent("onTLUpdateReadFeaturedStickersCustom",tlUpdateReadFeaturedStickers);
    }

    @Override
    protected void onTLUpdateRecentStickersCustom(TLUpdateRecentStickers tlUpdateRecentStickers) {
        logTLEvent("onTLUpdateRecentStickersCustom",tlUpdateRecentStickers);
    }

    @Override
    protected void onTLUpdateChannelWebPageCustom(TLUpdateChannelWebPage tlUpdateChannelWebPage) {
        logTLEvent("onTLUpdateChannelWebPageCustom",tlUpdateChannelWebPage);
    }

    @Override
    protected void onTLUpdatePhoneCallCustom(TLUpdatePhoneCall tlUpdatePhoneCall) {
        logTLEvent("onTLUpdatePhoneCallCustom",tlUpdatePhoneCall);
    }

    @Override
    protected void onTLUpdateDialogPinnedCustom(TLUpdateDialogPinned tlUpdateDialogPinned) {
        logTLEvent("onTLUpdateDialogPinnedCustom",tlUpdateDialogPinned);
    }

    @Override
    protected void onTLUpdatePinnedDialogsCustom(TLUpdatePinnedDialogs tlUpdatePinnedDialogs) {
        logTLEvent("onTLUpdatePinnedDialogsCustom",tlUpdatePinnedDialogs);
    }

    @Override
    protected void onTLUpdateBotWebhookJSONCustom(TLUpdateBotWebhookJSON tlUpdateBotWebhookJSON) {
        logTLEvent("onTLUpdateBotWebhookJSONCustom",tlUpdateBotWebhookJSON);
    }

    @Override
    protected void onTLUpdateBotWebhookJSONQueryCustom(TLUpdateBotWebhookJSONQuery tlUpdateBotWebhookJSONQuery) {
        logTLEvent("onTLUpdateBotWebhookJSONQueryCustom",tlUpdateBotWebhookJSONQuery);
    }

    @Override
    protected void onTLUpdateBotShippingQueryCustom(TLUpdateBotShippingQuery tlUpdateBotShippingQuery) {
        logTLEvent("onTLUpdateBotShippingQueryCustom",tlUpdateBotShippingQuery);
    }

    @Override
    protected void onTLUpdateBotPrecheckoutQueryCustom(TLUpdateBotPrecheckoutQuery tlUpdateBotPrecheckoutQuery) {
        logTLEvent("onTLUpdateBotPrecheckoutQueryCustom",tlUpdateBotPrecheckoutQuery);
    }

}
