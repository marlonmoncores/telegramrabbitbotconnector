package com.br.marlon.telegram.bot.messageforwarder.service;

import com.br.marlon.telegram.bot.messageforwarder.domain.FatMessage;
import com.br.marlon.telegram.bot.messageforwarder.filter.MessageFilter;
import com.br.marlon.telegram.bot.messageforwarder.filter.MessageFilterLoader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MessageFilterService {

    @Autowired
    private MessageFilterLoader filterLoader;

    public boolean checkFilterPass(FatMessage message){
        List<MessageFilter> filters = filterLoader.loadFilters();
        if(filters.isEmpty()){
            return true;
        }

        return filters.stream().anyMatch(filter -> filter.checkFilterPass(message));
    }
}
