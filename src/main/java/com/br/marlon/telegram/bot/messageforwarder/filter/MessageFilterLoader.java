package com.br.marlon.telegram.bot.messageforwarder.filter;

import com.br.marlon.telegram.bot.messageforwarder.filter.filters.MessageAccessHashFilter;
import com.br.marlon.telegram.bot.messageforwarder.filter.filters.MessageContentFilter;
import com.br.marlon.telegram.bot.messageforwarder.filter.filters.MessageNameFilter;
import com.br.marlon.telegram.bot.messageforwarder.filter.filters.MessageAuthorIdFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class MessageFilterLoader {

    @Autowired
    private FilterConfiguration filterConfiguration;

    private static List<MessageFilter> filtersList;
    
    public List<MessageFilter> loadFilters(){
        if(filtersList != null){
            return filtersList;
        }

        List<MessageFilter> filters = new ArrayList<>();
        if(filterConfiguration.getId() != null && !filterConfiguration.getId().isEmpty()){
            filters.add(new MessageAuthorIdFilter(filterConfiguration.getId()));
        }

        if(filterConfiguration.getAccessHash() != null && !filterConfiguration.getAccessHash().isEmpty()){
            filters.add(new MessageAccessHashFilter(filterConfiguration.getAccessHash()));
        }

        if(filterConfiguration.getName() != null && !filterConfiguration.getName().isEmpty()){
            filters.add(new MessageNameFilter(filterConfiguration.getName()));
        }

        if(filterConfiguration.getContent() != null && !filterConfiguration.getContent().isEmpty()){
            filters.add(new MessageContentFilter(filterConfiguration.getName()));
        }

        MessageFilterLoader.filtersList = filters;

        return filtersList;
    }
}
