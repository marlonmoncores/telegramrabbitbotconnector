package com.br.marlon.telegram.bot.messageforwarder.handler.managed;

import com.br.marlon.telegram.bot.database.entity.User;
import com.br.marlon.telegram.bot.messageforwarder.database.DatabaseManagerService;
import org.jetbrains.annotations.NotNull;
import org.telegram.api.user.TLAbsUser;
import org.telegram.api.user.TLUser;
import org.telegram.bot.handlers.interfaces.IUsersHandler;
import org.telegram.bot.services.BotLogger;

import java.util.List;


public class UserHandler implements IUsersHandler {
    private static final String LOGTAG = "USERSHANDLER";
    private final DatabaseManagerService databaseManager;

    public UserHandler(DatabaseManagerService databaseManager) {
        this.databaseManager = databaseManager;
    }


    @Override
    public void onUsers(@NotNull List<TLAbsUser> users) {
        users.forEach(this::onUser);
    }

    private void onUser(@NotNull TLAbsUser absUser) {
        if (absUser instanceof TLUser) {
            final TLUser tlUser = (TLUser) absUser;
            final User user = new User();

            user.setId(tlUser.getId());
            user.setUserHash(tlUser.getAccessHash());
            user.setFirstName(tlUser.getFirstName());
            user.setLastName(tlUser.getLastName());
            user.setPhone(tlUser.getPhone());
            user.setUserName(tlUser.getUserName());

            user.setMutualContact(tlUser.isMutualContact());
            user.setContact(tlUser.isContact());
            user.setSelf(tlUser.isSelf());
            user.setBot(tlUser.isBot());
            user.setDeleted(tlUser.isDeleted());

            databaseManager.save(user);
        } else {
            BotLogger.warn(LOGTAG, "Unsupported user type " + absUser);
        }
    }
}
