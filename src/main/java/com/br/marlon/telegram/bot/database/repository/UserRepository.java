package com.br.marlon.telegram.bot.database.repository;

import com.br.marlon.telegram.bot.database.entity.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, Integer> {
    
}
