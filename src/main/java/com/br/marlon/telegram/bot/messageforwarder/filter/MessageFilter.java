package com.br.marlon.telegram.bot.messageforwarder.filter;

import com.br.marlon.telegram.bot.messageforwarder.domain.FatMessage;

public interface MessageFilter {
    
    boolean checkFilterPass(FatMessage message);
}
