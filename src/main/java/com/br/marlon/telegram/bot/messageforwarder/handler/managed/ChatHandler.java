package com.br.marlon.telegram.bot.messageforwarder.handler.managed;

import com.br.marlon.telegram.bot.database.entity.Chat;
import com.br.marlon.telegram.bot.messageforwarder.database.DatabaseManagerService;
import org.telegram.api.chat.TLAbsChat;
import org.telegram.api.chat.TLChat;
import org.telegram.api.chat.TLChatForbidden;
import org.telegram.api.chat.channel.TLChannel;
import org.telegram.api.chat.channel.TLChannelForbidden;
import org.telegram.bot.handlers.interfaces.IChatsHandler;
import org.telegram.bot.services.BotLogger;

import java.util.List;

/**
 * @author Ruben Bermudez
 * @version 1.0
 * @brief Handler for received chats
 * @date 16 of October of 2016
 */
public class ChatHandler implements IChatsHandler {
    private static final String LOGTAG = "ChatHandler";

    private DatabaseManagerService databaseManager;

    public ChatHandler(DatabaseManagerService databaseManager) {
        this.databaseManager = databaseManager;
    }

    @Override
    public void onChats(List<TLAbsChat> chats) {
        chats.forEach(this::onAbsChat);
    }

    private void onAbsChat(TLAbsChat chat) {
        if (chat instanceof TLChannel) {
            onChannel((TLChannel) chat);
        } else if (chat instanceof TLChannelForbidden) {
            onChannelForbidden((TLChannelForbidden) chat);
        } else if (chat instanceof TLChat) {
            onChat((TLChat) chat);
        } else if (chat instanceof TLChatForbidden) {
            onChatForbidden((TLChatForbidden) chat);
        } else {
            BotLogger.warn(LOGTAG, "Unsupported chat type " + chat);
        }
    }

    private void onChatForbidden(TLChatForbidden tlChatForbidden) {
        Chat chat = new Chat();
        chat.setId(tlChatForbidden.getId());
        chat.setChannel(false);
        chat.setForbidden(true);
        chat.setTitle(tlChatForbidden.getTitle());
        databaseManager.save(chat);
    }


    private void onChat(TLChat tlChat) {
        Chat chat = new Chat();
        chat.setId(tlChat.getId());
        chat.setChannel(false);
        chat.setForbidden(false);
        chat.setTitle(tlChat.getTitle());
        databaseManager.save(chat);
    }


    private void onChannelForbidden(TLChannelForbidden tlChannelForbidden) {
        Chat chat = new Chat();
        chat.setId(tlChannelForbidden.getId());
        chat.setChannel(true);
        chat.setForbidden(true);
        chat.setTitle(tlChannelForbidden.getTitle());
        chat.setAccessHash(tlChannelForbidden.getAccessHash());
        databaseManager.save(chat);
    }

    private void onChannel(TLChannel tlChannel) {
        Chat chat = new Chat();
        chat.setId(tlChannel.getId());
        chat.setChannel(true);
        chat.setForbidden(false);
        chat.setTitle(tlChannel.getTitle());
        chat.setUsername(tlChannel.getUsername());
        chat.setAccessHash(tlChannel.getAccessHash());
        databaseManager.save(chat);
    }

}
