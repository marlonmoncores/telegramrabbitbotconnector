package com.br.marlon.telegram.bot.messageforwarder.filter;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.Set;

@Configuration
@ConfigurationProperties(prefix="bot.messageforwarder.filter")
public class FilterConfiguration {

    private Set<Integer> id;
    private Set<Long> accessHash;
    private Set<String> name;
    private Set<String> content;


    public Set<Integer> getId() {
        return id;
    }

    public void setId(Set<Integer> id) {
        this.id = id;
    }

    public Set<Long> getAccessHash() {
        return accessHash;
    }

    public void setAccessHash(Set<Long> accessHash) {
        this.accessHash = accessHash;
    }

    public Set<String> getName() {
        return name;
    }

    public void setName(Set<String> name) {
        this.name = name;
    }

    public Set<String> getContent() {
        return content;
    }

    public void setContent(Set<String> content) {
        this.content = content;
    }
}
