package com.br.marlon.telegram.bot.messageforwarder.event;

import com.br.marlon.telegram.bot.messageforwarder.domain.Message;

public class MessageForwarderEvent extends DomainEvent<Message> {

    public MessageForwarderEvent(Message message) {
        super(message);
    }
}
