package com.br.marlon.telegram.bot.messageforwarder.handler.managed;

import com.br.marlon.spring.SpringContext;
import com.br.marlon.telegram.bot.messageforwarder.domain.Message;
import com.br.marlon.telegram.bot.messageforwarder.domain.utils.TLMessageConverter;
import com.br.marlon.telegram.bot.messageforwarder.event.MessageForwarderEventPublisher;
import org.telegram.api.message.TLAbsMessage;
import org.telegram.api.update.TLUpdateChannelNewMessage;
import org.telegram.api.update.TLUpdateEditChannelMessage;
import org.telegram.api.update.TLUpdateEditMessage;
import org.telegram.api.update.TLUpdateNewMessage;
import org.telegram.api.updates.TLUpdateShortChatMessage;
import org.telegram.api.updates.TLUpdateShortMessage;
import org.telegram.bot.services.BotLogger;
import org.telegram.tl.TLObject;

import java.util.Optional;

public class MessageHandler {
    private static final String LOGTAG = "MESSAGEHANDLER";

    private MessageForwarderEventPublisher messageForwarderEventPublisher;

    public MessageHandler() {
        this.messageForwarderEventPublisher = SpringContext.getBean(MessageForwarderEventPublisher.class);
    }


    public void onMessage(TLUpdateShortMessage tlUpdateShortMessage) {
        convertMessage(tlUpdateShortMessage);
    }

    public void onMessage(TLUpdateNewMessage tlUpdateNewMessage) {
        convertMessage(tlUpdateNewMessage.getMessage());
    }

    public void onMessage(TLAbsMessage tlAbsMessage) {
        convertMessage(tlAbsMessage);
    }

    public void onMessage(TLUpdateShortChatMessage tlUpdateShortChatMessage){
        convertMessage(tlUpdateShortChatMessage);
    }

    public void onMessage(TLUpdateEditChannelMessage tlUpdateEditChannelMessage){
        convertMessage(tlUpdateEditChannelMessage.getMessage());
    }

    public void onMessage(TLUpdateEditMessage tlUpdateEditMessage){
        convertMessage(tlUpdateEditMessage.getMessage());
    }

    public void onMessage(TLUpdateChannelNewMessage tlUpdateChannelNewMessage){
        convertMessage(tlUpdateChannelNewMessage.getMessage());
    }

    private void convertMessage(TLObject tlObject){
        TLMessageConverter converter = new TLMessageConverter();
        Optional<Message> messageOptional = converter.convertMessage(tlObject);
        if(messageOptional.isPresent()) {
            messageForwarderEventPublisher.publishMessage(messageOptional.get());
        }else{
            BotLogger.warn(LOGTAG, "Message discarded: "+tlObject.toString());
        }
    }


}
