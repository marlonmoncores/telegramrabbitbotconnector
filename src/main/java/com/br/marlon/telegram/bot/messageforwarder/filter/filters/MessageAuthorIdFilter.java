package com.br.marlon.telegram.bot.messageforwarder.filter.filters;

import com.br.marlon.telegram.bot.messageforwarder.domain.FatMessage;
import com.br.marlon.telegram.bot.messageforwarder.filter.MessageFilter;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class MessageAuthorIdFilter implements MessageFilter {

    private final Set<Integer> idList;

    public MessageAuthorIdFilter(Set<Integer> idList) {
        this.idList = idList;
    }

    @Override
    public boolean checkFilterPass(FatMessage message) {
        List<Integer> listToSearch = new ArrayList<>();

        if(message.getChat() != null){
            listToSearch.add(message.getChat().getId());
        }
        if(message.getChannel() != null){
            listToSearch.add(message.getChannel().getId());
        }
        if(message.getFromUser() != null) {
            listToSearch.add(message.getFromUser().getId());
        }

        return listToSearch.stream().anyMatch(value -> idList.contains(value));
    }
}
