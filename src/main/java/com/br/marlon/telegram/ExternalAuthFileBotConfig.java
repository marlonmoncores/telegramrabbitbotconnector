package com.br.marlon.telegram;

import org.telegram.bot.structure.BotConfig;

public class ExternalAuthFileBotConfig extends BotConfig {

    private String phoneNumber;
    private String pathToFile;

    public ExternalAuthFileBotConfig(String pathToFile, String phoneNumber) {
        this.phoneNumber = phoneNumber;
        setAuthfile(pathToFile+phoneNumber + ".auth");
    }

    @Override
    public String getPhoneNumber() {
        return phoneNumber;
    }

    @Override
    public String getBotToken() {
        return null;
    }

    @Override
    public boolean isBot() {
        return false;
    }
}
