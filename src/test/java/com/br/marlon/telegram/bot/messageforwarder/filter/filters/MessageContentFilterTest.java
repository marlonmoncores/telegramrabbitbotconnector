package com.br.marlon.telegram.bot.messageforwarder.filter.filters;

import com.br.marlon.telegram.bot.messageforwarder.filter.MessageFilter;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class MessageContentFilterTest extends MessageFilterBaseTest{

    private MessageFilter filter = new MessageContentFilter(stringFilterList);


    @Test
    public void shouldPassTestWhenContentHasValue(){
        when(message.getMessage()).thenReturn("zzzzzzz sssssss aaaaaaa rrrrrr "+stringFilterList.iterator().next().toUpperCase()+"xxxxxx kkkkk eeeee");
        boolean filterResult = filter.checkFilterPass(message);
        Assert.assertTrue(filterResult);
    }

    @Test
    public void shouldNotPassTestWhenValuesAreNotInSet(){
        when(message.getMessage()).thenReturn("zzzzzzz sssssss aaaaaaa rrrrrr xxxxxx kkkkk eeeee");
        boolean filterResult = filter.checkFilterPass(message);
        Assert.assertFalse(filterResult);
    }

    @Test
    public void shouldNotPassTestWhenValuesAreNull(){
        when(message.getMessage()).thenReturn(null);
        boolean filterResult = filter.checkFilterPass(message);
        Assert.assertFalse(filterResult);
    }
}
