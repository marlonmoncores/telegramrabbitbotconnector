package com.br.marlon.telegram.bot.messageforwarder.filter.filters;

import com.br.marlon.telegram.bot.database.entity.Chat;
import com.br.marlon.telegram.bot.database.entity.User;
import com.br.marlon.telegram.bot.messageforwarder.domain.FatMessage;
import org.junit.Before;
import org.mockito.Mock;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import static org.mockito.Mockito.when;

public abstract class MessageFilterBaseTest {

    protected static final Set<Long> longFilterList;
    protected static final Set<String> stringFilterList;
    protected static final Set<Integer> integerFilterList;

    protected static final String firstName = "first";
    protected static final String lastName = "last";


    static{
        longFilterList = new HashSet<>();
        longFilterList.addAll(Arrays.asList(1L,2L,3L,4L));

        stringFilterList = new HashSet<>();
        stringFilterList.addAll(Arrays.asList("Fake Content", "other fake Content","some fake message", firstName+ " "+lastName));

        integerFilterList = new HashSet<>();
        integerFilterList.addAll(Arrays.asList(1,2,3,4));
    }

    @Mock
    protected FatMessage message;
    @Mock
    protected Chat chat;
    @Mock
    protected Chat channel;
    @Mock
    protected User user;

    @Before
    public void init(){
        when(message.getChat()).thenReturn(chat);
        when(message.getChannel()).thenReturn(channel);
        when(message.getFromUser()).thenReturn(user);
    }
}
