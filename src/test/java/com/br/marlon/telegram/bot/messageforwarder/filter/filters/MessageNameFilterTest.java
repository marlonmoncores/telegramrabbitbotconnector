package com.br.marlon.telegram.bot.messageforwarder.filter.filters;

import com.br.marlon.telegram.bot.messageforwarder.filter.MessageFilter;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class MessageNameFilterTest extends MessageFilterBaseTest{

    private MessageFilter filter = new MessageNameFilter(stringFilterList);

    private final String pre ="zzzzzzz sssssss aaaaaaa rrrrrr ";
    private final String pos ="xxxxxx kkkkk eeeee";


    @Test
    public void shouldPassTestWhenMessageHasChatFieldInSetByTitle(){
        when(message.getChat().getTitle()).thenReturn(stringFilterList.iterator().next().toUpperCase());
        boolean filterResult = filter.checkFilterPass(message);
        Assert.assertTrue(filterResult);
    }

    @Test
    public void shouldPassTestWhenMessageHasChatFieldInSetByUsername(){
        when(message.getChat().getUsername()).thenReturn(stringFilterList.iterator().next().toUpperCase());
        boolean filterResult = filter.checkFilterPass(message);
        Assert.assertTrue(filterResult);
    }




    @Test
    public void shouldPassTestWhenMessageHasChannelFieldInSetByTitle(){
        when(message.getChannel().getTitle()).thenReturn(stringFilterList.iterator().next().toLowerCase());
        boolean filterResult = filter.checkFilterPass(message);
        Assert.assertTrue(filterResult);
    }

    @Test
    public void shouldPassTestWhenMessageHasChannelFieldInSetByUsername(){
        when(message.getChannel().getUsername()).thenReturn(stringFilterList.iterator().next().toLowerCase());
        boolean filterResult = filter.checkFilterPass(message);
        Assert.assertTrue(filterResult);
    }




    @Test
    public void shouldPassTestWhenMessageHasUserInSet(){
        when(message.getFromUser().getUserName()).thenReturn(stringFilterList.iterator().next().toUpperCase());
        boolean filterResult = filter.checkFilterPass(message);
        Assert.assertTrue(filterResult);
    }

    @Test
    public void shouldPassTestWhenMessageHasUserInSetByFirstAndLastName(){
        when(message.getFromUser().getFirstName()).thenReturn(firstName);
        when(message.getFromUser().getLastName()).thenReturn(lastName);
        boolean filterResult = filter.checkFilterPass(message);
        Assert.assertTrue(filterResult);
    }

    @Test
    public void shouldNotTestWhenMessageHasUserInSetOnlyWithFirstName(){
        when(message.getFromUser().getFirstName()).thenReturn(firstName);
        when(message.getFromUser().getLastName()).thenReturn("ooops");
        boolean filterResult = filter.checkFilterPass(message);
        Assert.assertFalse(filterResult);
    }

    @Test
    public void shouldNotTestWhenMessageHasUserInSetOnlyWithLastName(){
        when(message.getFromUser().getFirstName()).thenReturn("ooops");
        when(message.getFromUser().getLastName()).thenReturn(lastName);
        boolean filterResult = filter.checkFilterPass(message);
        Assert.assertFalse(filterResult);
    }

    @Test
    public void shouldNotPassTestWhenValuesAreNotInSet(){
        when(message.getChat().getTitle()).thenReturn(pre+pos);
        when(message.getChannel().getTitle()).thenReturn(pre+pos);
        when(message.getFromUser().getUserName()).thenReturn(pre+pos);

        boolean filterResult = filter.checkFilterPass(message);
        Assert.assertFalse(filterResult);
    }

    @Test
    public void shouldNotPassTestWhenValuesAreNull(){
        when(message.getChat()).thenReturn(null);
        when(message.getChannel()).thenReturn(null);
        when(message.getFromUser()).thenReturn(null);

        boolean filterResult = filter.checkFilterPass(message);
        Assert.assertFalse(filterResult);
    }
}
