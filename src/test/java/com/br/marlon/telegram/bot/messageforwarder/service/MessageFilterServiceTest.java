package com.br.marlon.telegram.bot.messageforwarder.service;

import com.br.marlon.telegram.bot.messageforwarder.domain.FatMessage;
import com.br.marlon.telegram.bot.messageforwarder.filter.MessageFilter;
import com.br.marlon.telegram.bot.messageforwarder.filter.MessageFilterLoader;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class MessageFilterServiceTest {

    @Mock
    private MessageFilterLoader filterLoader;

    @InjectMocks
    private MessageFilterService messageFilterService;

    @Mock
    private FatMessage message;


    @Test
    public void shouldPassFilterIfAnyFilterReturnTrue(){
        List<MessageFilter> filters = mockFilterList(5,1);
        when(filterLoader.loadFilters()).thenReturn(filters);

        boolean response = messageFilterService.checkFilterPass(message);
        Assert.assertTrue(response);
    }

    @Test
    public void shouldNotPassFilterIfAllFilterReturnFalse(){
        List<MessageFilter> filters = mockFilterList(5,0);
        when(filterLoader.loadFilters()).thenReturn(filters);

        boolean response = messageFilterService.checkFilterPass(message);
        Assert.assertFalse(response);
    }

    @Test
    public void shouldPassFilterIfNoFilterExists(){
        List<MessageFilter> filters = mockFilterList(0,0);
        when(filterLoader.loadFilters()).thenReturn(filters);

        boolean response = messageFilterService.checkFilterPass(message);
        Assert.assertTrue(response);
    }



    private List<MessageFilter> mockFilterList(int totalFail, int totalPass){
        List<MessageFilter> mockList = new ArrayList<>(totalFail+totalPass);

        for(int i=0;i<totalFail;i++){
            mockList.add(mockFilter(false));
        }

        for(int i=0;i<totalPass;i++){
            mockList.add(mockFilter(true));
        }

        return mockList;
    }

    private MessageFilter mockFilter(boolean pass){
        MessageFilter messageFilterMock = Mockito.mock(MessageFilter.class);
        when(messageFilterMock.checkFilterPass(any())).thenReturn(pass);
        return messageFilterMock;
    }
}
