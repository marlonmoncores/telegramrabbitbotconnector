package com.br.marlon.telegram.bot.messageforwarder.filter.filters;

import com.br.marlon.telegram.bot.messageforwarder.filter.MessageFilter;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class MessageAuthorIdFilterTest extends MessageFilterBaseTest{

    private MessageFilter filter = new MessageAuthorIdFilter(integerFilterList);


    @Test
    public void shouldPassTestWhenMessageHasChatFieldInSet(){
        when(message.getChat().getId()).thenReturn(1);
        boolean filterResult = filter.checkFilterPass(message);
        Assert.assertTrue(filterResult);
    }

    @Test
    public void shouldPassTestWhenMessageHasChannelFieldInSet(){
        when(message.getChannel().getId()).thenReturn(2);
        boolean filterResult = filter.checkFilterPass(message);
        Assert.assertTrue(filterResult);
    }

    @Test
    public void shouldPassTestWhenMessageHasUserInSet(){
        when(message.getFromUser().getId()).thenReturn(3);
        boolean filterResult = filter.checkFilterPass(message);
        Assert.assertTrue(filterResult);
    }

    @Test
    public void shouldNotPassTestWhenValuesAreNotInSet(){
        when(message.getChat().getId()).thenReturn(10);
        when(message.getFromUser().getId()).thenReturn(11);
        when(message.getFromUser().getId()).thenReturn(12);

        boolean filterResult = filter.checkFilterPass(message);
        Assert.assertFalse(filterResult);

    }

    @Test
    public void shouldNotPassTestWhenValuesAreNull(){
        when(message.getChat()).thenReturn(null);
        when(message.getChannel()).thenReturn(null);
        when(message.getFromUser()).thenReturn(null);

        boolean filterResult = filter.checkFilterPass(message);
        Assert.assertFalse(filterResult);
    }



}
