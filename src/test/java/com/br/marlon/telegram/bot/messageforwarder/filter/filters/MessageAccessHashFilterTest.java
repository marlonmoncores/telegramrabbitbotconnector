package com.br.marlon.telegram.bot.messageforwarder.filter.filters;

import com.br.marlon.telegram.bot.messageforwarder.filter.MessageFilter;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class MessageAccessHashFilterTest extends MessageFilterBaseTest{

    private MessageFilter filter = new MessageAccessHashFilter(longFilterList);


    @Test
    public void shouldPassTestWhenMessageHasChatFieldInSet(){
        when(message.getChat().getAccessHash()).thenReturn(1l);
        boolean filterResult = filter.checkFilterPass(message);
        Assert.assertTrue(filterResult);
    }

    @Test
    public void shouldPassTestWhenMessageHasChannelFieldInSet(){
        when(message.getChannel().getAccessHash()).thenReturn(2l);
        boolean filterResult = filter.checkFilterPass(message);
        Assert.assertTrue(filterResult);
    }

    @Test
    public void shouldPassTestWhenMessageHasUserInSet(){
        when(message.getFromUser().getUserHash()).thenReturn(3l);
        boolean filterResult = filter.checkFilterPass(message);
        Assert.assertTrue(filterResult);
    }

    @Test
    public void shouldNotPassTestWhenValuesAreNotInSet(){
        when(message.getChat().getAccessHash()).thenReturn(10l);
        when(message.getChannel().getAccessHash()).thenReturn(11l);
        when(message.getFromUser().getUserHash()).thenReturn(12l);

        boolean filterResult = filter.checkFilterPass(message);
        Assert.assertFalse(filterResult);
    }

    @Test
    public void shouldNotPassTestWhenValuesAreNull(){
        when(message.getChat()).thenReturn(null);
        when(message.getChannel()).thenReturn(null);
        when(message.getFromUser()).thenReturn(null);

        boolean filterResult = filter.checkFilterPass(message);
        Assert.assertFalse(filterResult);
    }
}
