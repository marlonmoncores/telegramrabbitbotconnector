package com.br.marlon.telegram.bot.messageforwarder.filter;

import com.br.marlon.telegram.bot.messageforwarder.filter.filters.MessageAccessHashFilter;
import com.br.marlon.telegram.bot.messageforwarder.filter.filters.MessageAuthorIdFilter;
import com.br.marlon.telegram.bot.messageforwarder.filter.filters.MessageContentFilter;
import com.br.marlon.telegram.bot.messageforwarder.filter.filters.MessageNameFilter;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class MessageFilterLoaderTest {

    @Mock
    private FilterConfiguration filterConfiguration;

    @InjectMocks
    private MessageFilterLoader messageFilterLoader;

    @After
    public void reset(){
        ReflectionTestUtils.setField(messageFilterLoader, "filtersList", null);
    }


    @Test
    public void shouldReturnEmptyListWhenFiltersAreEmpty(){
        when(filterConfiguration.getId()).thenReturn(new HashSet<>());
        when(filterConfiguration.getAccessHash()).thenReturn(new HashSet<>());
        when(filterConfiguration.getName()).thenReturn(new HashSet<>());
        when(filterConfiguration.getContent()).thenReturn(new HashSet<>());

        List<MessageFilter> filters = messageFilterLoader.loadFilters();

        Assert.assertEquals(0, filters.size());
    }

    @Test
    public void shouldReturnEmptyListWhenFiltersAreNull(){
        when(filterConfiguration.getId()).thenReturn(null);
        when(filterConfiguration.getAccessHash()).thenReturn(null);
        when(filterConfiguration.getName()).thenReturn(null);
        when(filterConfiguration.getContent()).thenReturn(null);

        List<MessageFilter> filters = messageFilterLoader.loadFilters();

        Assert.assertEquals(0, filters.size());
    }


    @Test
    public void shouldReturnConcreteFilterTypeWhenListIsFilled(){
        Set mockSet = Mockito.mock(Set.class);
        when(mockSet.size()).thenReturn(1);

        when(filterConfiguration.getId()).thenReturn(mockSet);
        when(filterConfiguration.getAccessHash()).thenReturn(mockSet);
        when(filterConfiguration.getName()).thenReturn(mockSet);
        when(filterConfiguration.getContent()).thenReturn(mockSet);

        List<MessageFilter> filters = messageFilterLoader.loadFilters();

        Assert.assertEquals(4, filters.size());

        Set<Class> classes = new HashSet<>();
        classes.addAll(Arrays.asList(MessageAuthorIdFilter.class, MessageAccessHashFilter.class, MessageContentFilter.class, MessageNameFilter.class));

        for (MessageFilter filter: filters) {
            Assert.assertTrue(classes.remove(filter.getClass()));
        }
    }
}
